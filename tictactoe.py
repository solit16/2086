import datetime
from typing import List
from errors import NotReadyToStart

now = datetime.datetime.now()
list: str = []
class Game:
    def __init__(self):
        pass


    def filling(self, array):
        for i in range(9):
            array.append("-")
        return None


    def ask_if_user_ready_for_start(self):
        while True:
            ans = input("Готовы начать? (y/n): ")
            if ans == "y":
                return
            elif ans == "n":
                raise NotReadyToStart
            else:
                print("Не понимаю тебя")


    def decorator(self, func):
        print("Поле проверено")
        func(list)
        return None


    def hello(self):
        if 11 >= now.hour >= 4:
            return "Сегодня прекрасное утро, чтобы сыграть пару партий в крестики-нолики!"
        elif 18 >= now.hour >= 12:
            return "Прекрасный день для крестиков-ноликов!"
        elif 23 >= now.hour >= 19:
            return "Отличный вечер! Может, разнообразить его крестиками-ноликами?"
        else:
            return "Интересная ночь, неправда-ли? Мог бы и выспаться, вместо того, чтобы играть в крестики-нолики."


    def draw_current_position(self) -> str:
        print(list[0] + list[1] + list[2])
        print(list[3] + list[4] + list[5])
        print(list[6] + list[7] + list[8])
        return None


    def check(self, list: List):
        # проверка рядов
        if (list[0] == "x") and (list[1] == "x") and (list[2] == "x"):
            return 1
        if (list[3] == "x") and (list[4] == "x") and (list[5] == "x"):
            return 1
        if (list[6] == "x") and (list[7] == "x") and (list[8] == "x"):
            return 1

        if (list[0] == "o") and (list[1] == "o") and (list[2] == "o"):
            return 2
        if (list[3] == "o") and (list[4] == "o") and (list[5] == "o"):
            return 2
        if (list[6] == "o") and (list[7] == "o") and (list[8] == "o"):
            return 2

        # проверка столбцов
        if (list[0] == "x") and (list[3] == "x") and (list[6] == "x"):
            return 1
        if (list[1] == "x") and (list[4] == "x") and (list[7] == "x"):
            return 1
        if (list[2] == "x") and (list[5] == "x") and (list[8] == "x"):
            return 1

        if (list[0] == "o") and (list[3] == "o") and (list[6] == "o"):
            return 2
        if (list[1] == "o") and (list[4] == "o") and (list[7] == "o"):
            return 2
        if (list[2] == "o") and (list[5] == "o") and (list[8] == "o"):
            return 2

        # проверка диагоналей
        if (list[0] == "x") and (list[4] == "x") and (list[8] == "x"):
            return 1
        if (list[2] == "x") and (list[4] == "x") and (list[6] == "x"):
            return 1
        if (list[0] == "o") and (list[4] == "o") and (list[8] == "o"):
            return 2
        if (list[2] == "o") and (list[4] == "o") and (list[6] == "o"):
            return 2
        return 0



def game_start():
    game = Game()

    game.filling(list)
    print(game.hello())

    try:
        game.ask_if_user_ready_for_start()
    except NotReadyToStart as error:
        print(error)
        return
    else:
        print("Начнём...")

    game.draw_current_position()

    while True:
        try:
            move1 = int(input("Ход крестика: "))
            if move1 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move1 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move1 - 1] = "x"

    game.draw_current_position()

    while True:
        try:
            move2 = int(input("Ход нолика: "))
            if move2 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move2 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move2 - 1] = "o"

    game.draw_current_position()

    while True:
        try:
            move3 = int(input("Ход крестика: "))
            if move3 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move3 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move3 - 1] = "x"

    game.draw_current_position()

    while True:
        try:
            move4 = int(input("Ход нолика: "))
            if move4 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move4 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move4 - 1] = "o"

    game.draw_current_position()

    while True:
        try:
            move5 = int(input("Ход крестика: "))
            if move5 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move5 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move5 - 1] = "x"

    game.draw_current_position()

    game.decorator(game.check)

    if game.check(list) == 1:
        print("Победа крестика!")
        return
    elif game.check(list) == 2:
        print("Победа нолика!")
        return
    while True:
        try:
            move6 = int(input("Ход нолика: "))
            if move6 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move6 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move6 - 1] = "o"

    game.draw_current_position()

    game.decorator(game.check)

    if game.check(list) == 1:
        print("Победа крестика!")
        return
    elif game.check(list) == 2:
        print("Победа нолика!")

    while True:
        try:
            move7 = int(input("Ход крестика: "))
            if move7 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move7 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move7 - 1] = "x"

    game.draw_current_position()

    game.decorator(game.check)

    if game.check(list) == 1:
        print("Победа крестика!")
        return
    elif game.check(list) == 2:
        print("Победа нолика!")

    while True:
        try:
            move8 = int(input("Ход нолика: "))
            if move8 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move8 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move8 - 1] = "o"

    game.draw_current_position()

    game.decorator(game.check)

    if game.check(list) == 1:
        print("Победа крестика!")
        return
    elif game.check(list) == 2:
        print("Победа нолика!")

    while True:
        try:
            move9 = int(input("Ход крестика: "))
            if move9 > 9 or move1 < 1:
                print(
                    "Поля с таким индексом не существует. "
                    "Вводимые числа должны принадлежать промежутку [1;9] по целым значениям.")
                continue
            if list[move9 - 1] != "-":
                print("Это поле уже занято, ход невозможен.")
                continue
        except ValueError:
            print("Должно быть введено число")
        else:
            break

    list[move9 - 1] = "x"

    game.draw_current_position()

    game.decorator(game.check)

    if game.check(list) == 1:
        print("Победа крестика!")
        return
    elif game.check(list) == 2:
        print("Победа нолика!")
        return
    else:
        print("На доске ничья!")
        return


game_start()
