from tictactoe import filling, decorator, hello, draw_current_position, check, ask_if_user_ready_for_start
import pytest
import datetime

now = datetime.datetime.now()


# просто пустая функция для реализации теста функции decorator
def helping(a):
    return None


def test_filling_1():
    assert filling([]) is None


def test_filling_2():
    list_test = []
    filling(list_test)
    assert list_test == ["-", "-", "-", "-", "-", "-", "-", "-", "-", ]


def test_decorator():
    assert decorator(helping) is None


def test_hello():
    if 11 >= now.hour >= 4:
        assert hello() == "Сегодня прекрасное утро, чтобы сыграть пару партий в крестики-нолики!"
    elif 18 >= now.hour >= 12:
        assert hello() == "Прекрасный день для крестиков-ноликов!"
    elif 23 >= now.hour >= 19:
        assert hello() == "Отличный вечер! Может, разнообразить его крестиками-ноликами?"
    else:
        assert hello() == "Интересная ночь, неправда-ли? Мог бы и выспаться, вместо того, чтобы играть в" \
                          " крестики-нолики."


def test_draw_current_position():
    assert draw_current_position() is None


def test_check():
    assert check() == 1 or check() == 2 or check() == 0


def test_ask_if_user_ready_for_start():
    raise ask_if_user_ready_for_start() is None or ask_if_user_ready_for_start() == "Тогда зачем было запускать программу? -_-"
